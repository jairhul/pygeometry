install:
	pip install . --user

uninstall:
	pip uninstall pygeometry

develop:
	pip install --editable . --user
