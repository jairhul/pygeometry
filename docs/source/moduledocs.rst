===============
Module Contents
===============

This documentation is automatically generated by scanning all the source code. Parts
may be incomplete.

.. automodule:: pygeometry
    :members:
    :undoc-members:
    :show-inheritance:

pygeometry.gdml module
----------------------

.. automodule:: pygeometry.gdml
    :members:
    :undoc-members:
    :show-inheritance:

pygeometry.geant4 module
------------------------
   
.. automodule:: pygeometry.geant4
		:members:
		:undoc-members:
		:show-inheritance:
   
pygeometry.pycsg module
-----------------------
   
.. automodule:: pygeometry.pycsg
		:members:
		:undoc-members:
		:show-inheritance:

pygeometry.transformation module
--------------------------------
   
.. automodule:: pygeometry.transformation
		:members:
		:undoc-members:
		:show-inheritance:

pygeometry.vtk
--------------
   
.. automodule:: pygeometry.vtk
		:members:
		:undoc-members:
		:show-inheritance:
