============
Installation
============


Requirements
------------

 * pygeometry is developed exclusively for Python 2.7.


Installation
------------

To install pygeometry, simply run ``make install`` from the root pygeometry
directory.::

  cd /my/path/to/repositories/
  git clone http://bitbucket.org/jairhul/pygeometry
  cd pygeometry
  make install

Alternatively, run ``make develop`` from the same directory to ensure
that any local changes are picked up.
