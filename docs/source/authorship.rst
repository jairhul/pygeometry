==========
Authorship
==========

The following people have contributed to pygeometry:

* Stewart Boogert
* Andrey Abramov
* Alistair Butcher
* Stuart Walker
* Laurie Nevay
