pygeometry Documentation
========================

pygeometry is a package to create, load, write and visualise constructive solid geometry.

.. toctree::
   :maxdepth: 2

   licence
   authorship
   installation
   moduledocs


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
